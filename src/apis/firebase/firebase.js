import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";
import firebaseConfig from "../../configs/firebase";

firebase.initializeApp(firebaseConfig);
const googleProvider = new firebase.auth.GoogleAuthProvider();

export const signOut = function () {
    return firebase.auth().signOut();
};

export const signInWithGoogle = function () {
    return firebase.auth().signInWithPopup(googleProvider);
};

const db = firebase.firestore();
const storage = firebase.storage();

export { db, storage };
