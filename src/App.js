import React, { useState, useRef } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { connect } from "react-redux";
import Home from "./pages/Home/Home";
import Chat from "./pages/Chat/Chat";
import ChatRoom from "./pages/ChatRoom/ChatRoom";
import Profile from "./pages/Profile/Profile";
import Login from "./components/Login/Login";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import SideDrawer from "./components/SideDrawer/SideDrawer";

function App({ user }) {
    // console.log(user);

    const [sidebarOpened, setSidebarOpened] = useState(false);
    const [search, setSearch] = useState("");

    const toggleSidebar = () => setSidebarOpened(!sidebarOpened);

    const evenimente = useRef(null);

    return (
        <div className="App">
            <Router>
                {!user ? (
                    <Login />
                ) : (
                    <>
                        <Header
                            toggleSidebar={toggleSidebar}
                            setSearch={setSearch}
                        />
                        <SideDrawer
                            sidebarOpened={sidebarOpened}
                            toggleSidebar={toggleSidebar}
                        />
                        <Switch>
                            <Route
                                path="/"
                                exact
                                render={props => (
                                    <Home
                                        {...props}
                                        evenimente={evenimente}
                                        search={search}
                                    />
                                )}
                            />
                            <Route
                                path="/chat"
                                render={props => (
                                    <Chat {...props} search={search} />
                                )}
                            />
                            <Route path="/profile" component={Profile} />
                            <Route path="/room/:roomId" component={ChatRoom} />
                        </Switch>
                        <Footer />
                    </>
                )}
            </Router>
        </div>
    );
}

function mapStateToProps(state) {
    return {
        user: state.user.data
    };
}

export default connect(mapStateToProps)(App);
