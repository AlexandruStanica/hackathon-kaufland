import React from "react";
import { connect } from "react-redux";
import Card from "../Card/Card";
import SnackbarAlert from "../SnackbarAlert/SnackbarAlert";
import "./Cards.css";
import cards from "../CardsData";

function Cards({ evenimente, search }) {
    const mappedCards = cards
        .filter(card => {
            if (search === "") {
                return card;
            } else if (
                card.name.toLowerCase().includes(search.toLowerCase()) ||
                card.label.toLowerCase().includes(search.toLowerCase()) ||
                card.text.toLowerCase().includes(search.toLowerCase())
            ) {
                return card;
            } else {
                return null;
            }
        })
        .map((card, index) => {
            return (
                <Card
                    key={index}
                    id={card.id}
                    src={card.src}
                    text={card.text}
                    label={card.label}
                />
            );
        });

    return (
        <div className="cards">
            <h1 className="cards__title" ref={evenimente}>
                Evenimente
            </h1>
            <div className="cards__container">
                <div className="cards__wrapper">
                    {mappedCards.length === 0 ? <SnackbarAlert /> : mappedCards}
                </div>
            </div>
        </div>
    );
}

function mapStateToProps(state) {
    return {
        user: state.user.data
    };
}

export default connect(mapStateToProps)(Cards);
