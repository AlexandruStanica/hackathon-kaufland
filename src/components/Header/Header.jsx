import React, { useRef } from "react";
import { connect } from "react-redux";
import { Link, useLocation } from "react-router-dom";
import { Avatar, Button } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import MenuIcon from "@material-ui/icons/Menu";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { logoutUser } from "../../redux/user/userActions";
import { openSnackbar } from "../../redux/snackbar/snackbarActions";
import { animateScroll as scroll } from "react-scroll";
import "./Header.css";

function Header({
    user,
    toggleSidebar,
    search,
    setSearch,
    signOut,
    openSnackbar
}) {
    const location = useLocation();

    const inputEl = useRef(null);

    function focusSearchInput() {
        inputEl.current.focus();
    }

    const scrollToBottom = e => {
        e.preventDefault();
        if (location.pathname === "/") {
            scroll.scrollToBottom({
                duration: 0,
                delay: 0
            });

            openSnackbar();
        }
    };

    return (
        <header className="header">
            <div className="header__left">
                <Link to="/profile">
                    <Avatar
                        className="header__avatar"
                        src={user?.photoURL}
                        alt={user?.displayName}
                    />
                </Link>
            </div>
            <div className="header__search">
                <SearchIcon />
                <form onSubmit={e => scrollToBottom(e)}>
                    <input
                        ref={inputEl}
                        type="text"
                        placeholder="Caută..."
                        className="search-input"
                        value={search}
                        onChange={e => setSearch(e.target.value)}
                    />
                </form>
            </div>
            <div className="header__right">
                <nav className="header__navigation">
                    <ul className="header__navigation--items">
                        <li>
                            <Link
                                to="/"
                                className="header__link"
                                onClick={focusSearchInput}
                            >
                                Acasă
                            </Link>
                        </li>
                        <li>
                            <Link
                                to="/chat"
                                className="header__link header__link__last"
                            >
                                Mesaje
                            </Link>
                        </li>
                        <li>
                            <Button onClick={signOut}>
                                <ExitToAppIcon className="logout__icon" />
                                Deconectează-te
                            </Button>
                        </li>
                    </ul>

                    <MenuIcon
                        style={{ color: "white" }}
                        onClick={toggleSidebar}
                        className="hamburger-menu"
                    />
                </nav>
            </div>
        </header>
    );
}

function mapStateToProps(state) {
    return {
        user: state.user.data
    };
}

function mapDispatchToProps(dispatch) {
    return {
        signOut: () => dispatch(logoutUser()),
        openSnackbar: () => dispatch(openSnackbar())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
