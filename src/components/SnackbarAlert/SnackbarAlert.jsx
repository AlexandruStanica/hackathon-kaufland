import React from "react";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { makeStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { closeSnackbar } from "../../redux/snackbar/snackbarActions";

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles(theme => ({
    root: {
        width: "100%",
        "& > * + *": {
            marginTop: theme.spacing(2)
        }
    }
}));

function SnackbarAlert({ open, closeSnackbar }) {
    const classes = useStyles();

    const handleClose = (event, reason) => {
        if (reason === "clickaway") {
            return;
        }

        closeSnackbar();
    };

    return (
        <div className={classes.root}>
            <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="info">
                    Nu au fost găsite evenimente!
                </Alert>
            </Snackbar>
        </div>
    );
}

function mapStateToProps(state) {
    return {
        open: state.snackbar.open
    };
}

function mapDispatchToProps(dispatch) {
    return {
        closeSnackbar: () => dispatch(closeSnackbar())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SnackbarAlert);
