import VaUrma from "../assets/images/va-urma.png";
import BlindDates from "../assets/images/blind-dates.png";

const cards = [
    {
        id: 1,
        src: VaUrma,
        name: "Va Urma",
        text: "Fii cu ochii pe noi! Echipa noastră îți pregătește amintiri de neuitat. Vor apărea în curând noi evenimente.",
        label: "Dezvoltare personala"
    },
    {
        id: 2,
        src: BlindDates,
        name: "Blind Dates",
        text: "Evenimentul, care este dedicat tuturor, oferă oportunitatea de a găsi cât mai mulți oameni cu interese comune, scopul acestuia fiind legarea unor prietenii de durată.",
        label: "Social"
    },
    {
        id: 3,
        src: VaUrma,
        name: "Va Urma",
        text: "Fii cu ochii pe noi! Echipa noastră îți pregătește amintiri de neuitat. Vor apărea în curând noi evenimente.",
        label: "Aventura"
    }
];

export default cards;
