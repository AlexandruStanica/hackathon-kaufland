import React from "react";
import { connect } from "react-redux";
import "./Message.css";

function Message({ user, message, userName, userImage, uid }) {
    const messageClass = uid === user.uid ? "sent" : "received";

    return (
        <div className={`message ${messageClass}`}>
            <img src={userImage} alt="" />
            <div className="message__info">
                <h4>{userName}</h4>
                <p>{message}</p>
            </div>
        </div>
    );
}

function mapStateToProps(state) {
    return {
        user: state.user.data
    };
}

export default connect(mapStateToProps)(Message);
