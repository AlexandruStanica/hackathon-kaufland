import React from "react";
import CloseIcon from "@material-ui/icons/Close";
import { SideDrawerData } from "../SideDrawerData";
import { Link } from "react-router-dom";
import { Button } from "@material-ui/core";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { logoutUser } from "../../redux/user/userActions";
import { connect } from "react-redux";
import "./SideDrawer.css";

function SideDrawer({ sidebarOpened, toggleSidebar, signOut }) {
    return (
        <nav className={sidebarOpened ? "nav-menu active" : "nav-menu"}>
            <ul className="nav-menu-items" onClick={toggleSidebar}>
                <li className="navbar-toggle">
                    <CloseIcon
                        style={{ color: "white" }}
                        className="close-icon"
                    />
                </li>
                {SideDrawerData.map((item, index) => {
                    return (
                        <li key={index} className="nav-text">
                            <Link to={item.path}>
                                {item.icon}
                                <span>{item.title}</span>
                            </Link>
                        </li>
                    );
                })}
                <Button onClick={signOut}>
                    <ExitToAppIcon className="logout__icon" />
                    Deconectează-te
                </Button>
            </ul>
        </nav>
    );
}

function mapDispatchToProps(dispatch) {
    return {
        signOut: () => dispatch(logoutUser())
    };
}

export default connect(null, mapDispatchToProps)(SideDrawer);
