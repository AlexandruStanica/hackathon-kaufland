import React from "react";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import "./Carousel.css";
import HeroText from "../HeroText/HeroText";

function Carousel({ scrollDown }) {
    return (
        <OwlCarousel
            items={1}
            className="owl-theme"
            loop
            autoplay
            animateOut="fadeOut"
            dots={false}
        >
            <div className="hero-1">
                <HeroText
                    scrollDown={scrollDown}
                    title="Împreună mai puternici"
                />
            </div>
            <div className="hero-2">
                <HeroText
                    scrollDown={scrollDown}
                    title="Împreună mai puternici"
                />
            </div>
            <div className="hero-3">
                <HeroText
                    scrollDown={scrollDown}
                    title="Împreună mai puternici"
                />
            </div>
        </OwlCarousel>
    );
}

export default Carousel;
