import React from "react";
import { Link } from "react-router-dom";
import "./HeroText.css";

function HeroText({ scrollDown, title }) {
    return (
        <div className="hero-text">
            <span className="subtitle">Bine ai venit!</span>
            <h1 className="title">{title}</h1>
            <p className="hero-paragraph">
                Participarea la diverse evenimente ne ajută la învățarea din
                experiențele altora și la crearea de noi prietenii între
                angajații oricărei firme, ceea ce conduce la o mai bună
                integrare a tuturor.
            </p>

            <div className="hero-buttons">
                <Link to="/" className="primary-btn" onClick={scrollDown}>
                    Evenimente
                </Link>
                <Link to="/profile" className="secondary-btn">
                    Profilul tău
                </Link>
            </div>
        </div>
    );
}

export default HeroText;
