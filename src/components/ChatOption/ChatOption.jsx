import React from "react";
import { useHistory } from "react-router";
import { db } from "../../apis/firebase/firebase";
import "./ChatOption.css";
import ChatIcon from "@material-ui/icons/Chat";
import * as firebase from "firebase/app";

function ChatOption({ Icon, title, id, addRoomOption }) {
    const history = useHistory();

    const selectRoom = () => {
        const roomPassword = prompt("Introduceti parola");

        if (roomPassword === id) {
            history.push(`/room/${id}`);
        }
    };

    const addRoom = () => {
        const roomName = prompt("Introduceți numele camerei");

        if (roomName) {
            db.collection("rooms").add({
                name: roomName,
                timestamp: firebase.firestore.FieldValue.serverTimestamp()
            });
        }
    };

    return (
        <div
            className={`chatOption ${addRoomOption ? "addRoomOption" : ""}`}
            onClick={addRoomOption ? addRoom : selectRoom}
        >
            {Icon && <Icon className="chatOption__icon--main" />}
            {Icon ? (
                <h3>{title}</h3>
            ) : (
                <h3 className="chatOption__room">
                    <span className="chatOption__icon">
                        <ChatIcon />
                    </span>
                    {title}
                </h3>
            )}
        </div>
    );
}

export default ChatOption;
