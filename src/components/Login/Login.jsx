import React from "react";
import Logo from "../../assets/images/logo.png";
import { Button } from "@material-ui/core";
import "./Login.css";
import { connect } from "react-redux";
import { loginGoogle } from "../../redux/user/userActions";
import { useHistory } from "react-router-dom";

function Login({ signInWithGoogle }) {
    const history = useHistory();

    function handleLoginClick() {
        signInWithGoogle();
        history.push("/profile");
    }

    return (
        <div className="login">
            <div className="login__container">
                <img src={Logo} alt="Disable IT" className="logo" />
                <h1>Bine ai venit!</h1>
                <Button onClick={handleLoginClick}>
                    Conectează-te cu Google
                </Button>
            </div>
        </div>
    );
}

function mapDispatchToProps(dispatch) {
    return {
        signInWithGoogle: () => dispatch(loginGoogle())
    };
}

export default connect(null, mapDispatchToProps)(Login);
