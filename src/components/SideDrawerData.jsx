import React from "react";
import HomeIcon from "@material-ui/icons/Home";
import ChatIcon from "@material-ui/icons/Chat";

export const SideDrawerData = [
    {
        title: "Acasă",
        path: "/",
        icon: <HomeIcon />
    },
    {
        title: "Mesaje",
        path: "/chat",
        icon: <ChatIcon />
    }
];
