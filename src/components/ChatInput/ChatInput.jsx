import React, { useState } from "react";
import { connect } from "react-redux";
import { db } from "../../apis/firebase/firebase";
import "./ChatInput.css";
import * as firebase from "firebase/app";

function ChatInput({ user, roomId, lastMessage }) {
    const [input, setInput] = useState("");

    const sendMessage = e => {
        e.preventDefault();

        if (roomId && input !== "") {
            db.collection("rooms").doc(roomId).collection("messages").add({
                message: input,
                timestamp: firebase.firestore.FieldValue.serverTimestamp(),
                userName: user.displayName,
                userImage: user.photoURL,
                uid: user.uid
            });

            setInput("");

            lastMessage.current.scrollIntoView({ behavior: "smooth" });
        }
    };

    return (
        <div className="chatInput">
            <form>
                <input
                    type="text"
                    value={input}
                    onChange={e => setInput(e.target.value)}
                    placeholder="Scrie un mesaj..."
                />
                <button type="submit" onClick={sendMessage}>
                    SEND
                </button>
            </form>
        </div>
    );
}

function mapStateToProps(state) {
    return {
        user: state.user.data
    };
}

export default connect(mapStateToProps)(ChatInput);
