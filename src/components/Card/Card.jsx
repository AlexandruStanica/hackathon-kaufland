import React, { useState } from "react";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import FavoriteIcon from "@material-ui/icons/Favorite";
import "./Card.css";
import { connect } from "react-redux";
import { emptyHeart, fillHeart } from "../../redux/hearts/heartsActions";

function Card({ id, src, text, label, myHearts, emptyHeart, fillHeart }) {
    const [heart, setHeart] = useState({ id });

    const heartIsFull = myHearts.find(myHeart => myHeart.id === heart.id);

    const changeHeart = id => {
        heartIsFull
            ? emptyHeart({
                  id
              }) && alert("Evenimentul a fost eliminat de la favorite!")
            : fillHeart({
                  heart: {
                      bool: true,
                      id,
                      src,
                      text,
                      label
                  }
              }) && alert("Evenimentul a fost adăugat de la favorite!");
    };

    return (
        <div>
            <li className="cards__item">
                <div className="cards__item__link">
                    <figure
                        className="cards__item__pic-wrap"
                        data-category={label}
                    >
                        <img
                            src={src}
                            alt="Travel"
                            className="cards__item__img"
                        />
                    </figure>
                    <div className="cards__item__info">
                        <h5 className="cards__item__text">{text}</h5>
                        <div onClick={() => changeHeart(id)}>
                            {heartIsFull ? (
                                <FavoriteIcon className="favorite__icon" />
                            ) : (
                                <FavoriteBorderIcon className="favorite__border__icon" />
                            )}
                        </div>
                    </div>
                </div>
            </li>
        </div>
    );
}

function mapStateToProps(state) {
    return {
        myHearts: state.hearts.myHearts
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fillHeart: payload => dispatch(fillHeart(payload)),
        emptyHeart: payload => dispatch(emptyHeart(payload))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Card);
