import React, { useState, useRef } from "react";
import "./EditModal.css";
import CloseIcon from "@material-ui/icons/Close";
import { db, storage } from "../../apis/firebase/firebase";
import firebase from "firebase/app";
import { Button } from "@material-ui/core";
import { connect } from "react-redux";
import UploadIcon from "@material-ui/icons/CloudUpload";

function EditModal({
    editModal,
    setEditModal,
    image,
    setImage,
    setImageUrl,
    user
}) {
    const editModalRef = useRef(null);

    const [progress, setProgress] = useState(0);
    const [fileName, setFileName] = useState("");

    const closeEditModal = e => {
        if (editModalRef.current === e.target) {
            setEditModal(false);
        }
    };

    const handleChange = e => {
        if (e.target.files[0]) {
            setImage(e.target.files[0]);
            setFileName(e.target.files[0].name);
        }
    };

    const handleUpload = () => {
        const uploadTask = storage.ref(`images/${image?.name}`).put(image);

        uploadTask.on(
            "state_changed",
            snapshot => {
                // Progress function...
                const progress = Math.round(
                    (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                );
                setProgress(progress);
            },
            error => {
                // Error function...
                console.log(error);
            },
            () => {
                // Complete function...
                storage
                    .ref("images")
                    .child(image.name)
                    .getDownloadURL()
                    .then(url => {
                        // Post image inside db
                        db.collection("posts").doc(user.uid).set({
                            timestamp:
                                firebase.firestore.FieldValue.serverTimestamp(),
                            imageUrl: url
                        });

                        setProgress(0);
                        setImage(null);
                        setImageUrl(url);
                        setEditModal(false);
                    });
            }
        );
    };

    return (
        <>
            {editModal ? (
                <div className="edit-modal">
                    <div
                        className="edit-modal-background"
                        ref={editModalRef}
                        onClick={closeEditModal}
                    >
                        <div className="edit-modal-wrapper">
                            <CloseIcon
                                className="close-icon"
                                onClick={() => setEditModal(false)}
                            />
                            <div className="edit-modal-content">
                                <div className="imageupload">
                                    <progress
                                        className="imageupload__progress"
                                        value={progress}
                                        max="100"
                                    />
                                    <input
                                        type="file"
                                        name="file"
                                        id="file"
                                        className="inputfile"
                                        onChange={handleChange}
                                    />
                                    <label htmlFor="file">
                                        {fileName
                                            ? fileName
                                            : "Încarcă o fotografie"}
                                    </label>
                                    <Button
                                        onClick={handleUpload}
                                        className="upload-btn"
                                    >
                                        <UploadIcon className="upload-icon" />
                                        Upload
                                    </Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            ) : null}
        </>
    );
}

function mapStateToProps(state) {
    return {
        user: state.user.data
    };
}

export default connect(mapStateToProps)(EditModal);
