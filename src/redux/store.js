import { createStore, combineReducers, applyMiddleware } from "redux";
import { userReducer } from "./user/userReducer";
import { snackbarReducer } from "./snackbar/snackbarReducer";
import { heartsReducer } from "./hearts/heartsReducer";
import thunk from "redux-thunk";
import logger from "redux-logger";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

const rootReducer = combineReducers({
    user: userReducer,
    snackbar: snackbarReducer,
    hearts: heartsReducer
});

const persistConfig = {
    key: "user",
    storage: storage,
    whitelist: ["user", "hearts"]
};

const pReducer = persistReducer(persistConfig, rootReducer);

const middlewares = [thunk];
if (process.env.NODE_ENV === "development") {
    middlewares.push(logger);
}

const store = createStore(pReducer, applyMiddleware(...middlewares));

const persistor = persistStore(store);

export { persistor, store };
