export function fillHeart(payload) {
    return {
        type: "FILL_HEART",
        payload
    };
}

export function emptyHeart(payload) {
    return {
        type: "EMPTY_HEART",
        payload
    };
}
