const initialState = {
    myHearts: []
};

export function heartsReducer(state = initialState, action) {
    switch (action.type) {
        case "FILL_HEART":
            return {
                ...state,
                myHearts: [...state.myHearts, action.payload.heart]
            };
        case "EMPTY_HEART":
            const filteredHearts = state.myHearts.filter(myHeart => {
                return myHeart.id !== action.payload.id;
            });

            return {
                ...state,
                myHearts: filteredHearts
            };
        default:
            return state;
    }
}
