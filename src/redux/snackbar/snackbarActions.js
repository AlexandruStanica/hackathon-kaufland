export function openSnackbar() {
    return {
        type: "OPEN_SNACKBAR"
    };
}

export function closeSnackbar() {
    return {
        type: "CLOSE_SNACKBAR"
    };
}
