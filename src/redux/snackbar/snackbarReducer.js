const initialState = {
    open: false
};

export function snackbarReducer(state = initialState, action) {
    switch (action.type) {
        case "OPEN_SNACKBAR":
            return {
                ...state,
                open: true
            };
        case "CLOSE_SNACKBAR":
            return {
                ...state,
                open: false
            };
        default:
            return state;
    }
}
