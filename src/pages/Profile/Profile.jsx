import React, { useEffect, useState } from "react";
import "./Profile.css";
import DefaultImage from "../../assets/images/profile-default.jpg";
import { connect } from "react-redux";
import EditIcon from "@material-ui/icons/Edit";
import { db } from "../../apis/firebase/firebase";
import EditModal from "../../components/EditModal/EditModal";
import Card from "../../components/Card/Card";
import Spinner from "react-spinkit";

function Profile({ user, favoriteCards }) {
    const [editModal, setEditModal] = useState(false);
    const [image, setImage] = useState(null);
    const [imageUrl, setImageUrl] = useState(null);
    const [imageLoaded, setImageLoaded] = useState(false);

    useEffect(() => {
        db.collection("users")
            .doc(user.uid)
            .get()
            .then(doc => {
                if (doc.data() === undefined) {
                    db.collection("users").doc(user.uid).set({
                        name: user.displayName,
                        email: user.email,
                        isAdmin: false
                    });
                }
            });
    }, []);

    useEffect(() => {
        db.collection("posts")
            .doc(user.uid)
            .get()
            .then(doc => {
                if (doc.data() === undefined) {
                    setImageUrl(DefaultImage);
                } else {
                    setImageUrl(doc.data().imageUrl);
                }
            });
    }, [user.uid, imageUrl]);

    console.log(user);

    return (
        <div className="profile">
            <div className="profile__card">
                <div className="profile__card__left">
                    <div className="smooth-image-wrapper">
                        <img
                            src={imageUrl}
                            alt=""
                            className={`profile__card__picture smooth-image image-${
                                imageLoaded ? "visible" : "hidden"
                            }`}
                            onLoad={() => setImageLoaded(true)}
                        />
                        {!imageLoaded && (
                            <div className="smooth-preloader">
                                <Spinner
                                    name="ball-spin-fade-loader"
                                    color="purple"
                                    fadeIn="none"
                                />
                            </div>
                        )}
                    </div>
                    <h2>{user?.displayName}</h2>
                    <button onClick={() => setEditModal(true)}>
                        <EditIcon className="edit-icon" />
                        Editează
                    </button>
                </div>
                <div className="profile__card__right">
                    <div className="mountains">
                        <h2>Mountains</h2>
                    </div>
                    <div className="profile__cards">
                        {favoriteCards.map(card => {
                            if (card.bool) {
                                return (
                                    <Card
                                        key={card.id}
                                        id={card.id}
                                        src={card.src}
                                        text={card.text}
                                        label={card.label}
                                    />
                                );
                            } else {
                                return null;
                            }
                        })}
                    </div>
                </div>
            </div>
            <EditModal
                editModal={editModal}
                setEditModal={setEditModal}
                image={image}
                setImage={setImage}
                setImageUrl={setImageUrl}
            />
        </div>
    );
}

function mapStateToProps(state) {
    return {
        user: state.user.data,
        favoriteCards: state.hearts.myHearts
    };
}

export default connect(mapStateToProps)(Profile);
