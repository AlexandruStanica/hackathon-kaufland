import React from "react";
import "./Home.css";
import Carousel from "../../components/Carousel/Carousel";
import Cards from "../../components/Cards/Cards";

function Home({ evenimente, search }) {
    const scrollDown = () => evenimente.current.scrollIntoView();

    return (
        <div className="home">
            <Carousel scrollDown={scrollDown} />
            <Cards evenimente={evenimente} search={search} />
        </div>
    );
}

export default Home;
