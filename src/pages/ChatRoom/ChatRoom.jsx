import React, { useEffect, useRef, useState } from "react";
import { useParams } from "react-router-dom";
import { db } from "../../apis/firebase/firebase";
import ChatIcon from "@material-ui/icons/Chat";
import "./ChatRoom.css";
import Message from "../../components/Message/Message";
import ChatInput from "../../components/ChatInput/ChatInput";

function ChatRoom() {
    const lastMessage = useRef(null);
    const { roomId } = useParams();
    const [roomDetails, setRoomDetails] = useState(null);
    const [roomMessages, setRoomMessages] = useState([]);

    useEffect(() => {
        lastMessage.current.scrollIntoView({ behavior: "smooth" });
    }, [roomMessages]);

    useEffect(() => {
        if (roomId) {
            db.collection("rooms")
                .doc(roomId)
                .onSnapshot(snapshot => setRoomDetails(snapshot.data()));
        }

        db.collection("rooms")
            .doc(roomId)
            .collection("messages")
            .orderBy("timestamp", "asc")
            .limitToLast(20)
            .onSnapshot(snapshot =>
                setRoomMessages(snapshot.docs.map(doc => doc.data()))
            );
    }, [roomId]);

    return (
        <div className="chatRoom">
            <div className="chatRoom__header">
                <div className="chatRoom__headerLeft">
                    <h4 className="chatRoom__roomName">
                        <div>
                            <ChatIcon />
                            {roomDetails?.name}
                        </div>
                    </h4>
                </div>
            </div>

            <div className="chatRoom__messages">
                {roomMessages.map(
                    ({ message, timestamp, userName, userImage, uid }) => (
                        <Message
                            message={message}
                            userName={userName}
                            userImage={userImage}
                            uid={uid}
                            key={timestamp}
                        />
                    )
                )}

                <div ref={lastMessage} className="lastMessage"></div>
            </div>

            <ChatInput lastMessage={lastMessage} roomId={roomId} />
        </div>
    );
}

export default ChatRoom;
