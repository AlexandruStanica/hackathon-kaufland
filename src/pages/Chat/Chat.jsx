import React, { useEffect, useState } from "react";
import "./Chat.css";
import AddIcon from "@material-ui/icons/Add";
import ChatOption from "../../components/ChatOption/ChatOption";
import { db } from "../../apis/firebase/firebase";
import { connect } from "react-redux";

function Chat({ search, user }) {
    const [rooms, setRooms] = useState([]);
    const [isAdmin, setIsAdmin] = useState(null);

    useEffect(() => {
        db.collection("rooms")
            .orderBy("timestamp", "desc")
            .onSnapshot(snapshot =>
                setRooms(
                    snapshot.docs.map(doc => ({
                        id: doc.id,
                        name: doc.data().name
                    }))
                )
            );
    }, []);

    db.collection("users")
        .doc(user.uid)
        .get()
        .then(doc => setIsAdmin(doc.data().isAdmin));

    return (
        <div className="chat">
            <h1 className="chat__title">Mesaje</h1>
            {isAdmin && (
                <ChatOption
                    Icon={AddIcon}
                    addRoomOption
                    title="Adaugă cameră"
                />
            )}

            {rooms.map(room => (
                <ChatOption title={room.name} id={room.id} key={room.id} />
            ))}
        </div>
    );
}

function mapStateToProps(state) {
    return {
        user: state.user.data
    };
}

export default connect(mapStateToProps)(Chat);
